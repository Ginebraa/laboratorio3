# Macros

CC     = gcc
CFLAGS = -c
OFLAGS = -o

DIR_OUTPUT        = ./build
DIR_SRC           = ./src
DIR_TMP			  = ./tmp
NOMBRE_EJECUTABLE_1 = programa1
NOMBRE_EJECUTABLE_2 = programa2


all: release

# Compilación principal del ejecutable
release:
	@echo 'Compilando target: $@'
	@echo "Creando el directorio ./build"
	mkdir -p $(DIR_OUTPUT)
	mkdir -p $(DIR_TMP)
	@echo ' '
	@echo "####################################"
	@echo "Compilando programas"
	$(CC)  $(CFLAGS) $(OFLAGS) $(DIR_TMP)/programa1.o $(DIR_SRC)/programa1.c 
	$(CC)  $(CFLAGS) $(OFLAGS) $(DIR_TMP)/programa2.o $(DIR_SRC)/programa2.c

	$(CC) $(OFLAGS) $(DIR_OUTPUT)/programa1 $(DIR_TMP)/programa1.o 
	$(CC) $(OFLAGS) $(DIR_OUTPUT)/programa2 $(DIR_TMP)/programa2.o 


	@echo ' '
	@echo "####################################################"
	@echo " Los programas han sido compilados correctamente"
	@echo "####################################################"
	
	@echo ' '
	@echo ">>> Programa compilado en el directorio $(DIR_OUTPUT)"
	@echo ">>> Ingrese al directorio '"$(DIR_OUTPUT)"' mediante el comando cd"
	@echo ">>> Ejecute los programas haciendo: ./$(NOMBRE_EJECUTABLE_1) o /$(NOMBRE_EJECUTABLE_1) "
	@echo ">>> ( Si desea visualizar el tiempo de ejecuciÃ³n anteponga: 'time' ) "
	@echo ' '

clear:
	rm -rf $(DIR_OUTPUT)/*
	rm -rf $(DIR_TMP)/*
	-@echo ' ' 
	@echo ">>> Archivos compilados y objetos eliminados correctamente"
